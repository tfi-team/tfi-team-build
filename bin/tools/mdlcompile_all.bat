@echo off
REM setlocal enabledelayedexpansion
(
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\chimneypipe_cluster02a\compile.qc
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\chimneypipe_cluster02b\compile.qc
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\chimneysmokestack_cluster01a\compile.qc
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\pipe_cap001\compile.qc
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\pipe_cap002\compile.qc
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\pipe_cap005b\compile.qc
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\pipe_cap005c\compile.qc
bin\studiomdl -game tf2 tf2\modelsrc\props2fort\pipe_runoff005\compile.qc
) > "%~dpn0.txt"
pause