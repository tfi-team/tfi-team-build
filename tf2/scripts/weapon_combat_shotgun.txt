// aVe teh_lolweap

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"        "ArcWelder"
	"viewmodel"        "models/weapons/v_gm_ar.mdl"
	"playermodel"      "models/weapons/w_gm_ar.mdl"	//FIXME: 
	"anim_prefix"      "avegun"
	"bucket"           "2"
	"bucket_position"  "0"

	"clip_size"        "6"
	"clip2_size"       "-1"

	"default_clip"     "6"
	"default_clip2"    "-1"

	"primary_ammo"     "Bullets"


	"weight"           "2"
	"item_flags"       "0"

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"ammo"
		{
				"file"    "sprites/a_icons1"
				"x"       "55"
				"y"       "60"
				"width"   "73"
				"height"  "15"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			  "0"
				"y"       "48"
				"width"   "24"
				"height"  "24"
		}
		"autoaim"
		{
				"file"    "sprites/crosshairs"
				"x"       "0"
				"y"       "48"
				"width"   "24"
				"height"  "24"
		}	
	}
	SoundData
	{
		"reload"			"TF2.Reload"
		"empty"			"AlienEscort.Death"
		"single_shot"		"Rocketlauncher.Fire"
	}
}
