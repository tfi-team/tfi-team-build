"Resource/HudLayout.res"
{
	HudMinimap
	{
		"fieldName"		"HudMinimap"
		"xpos"	"16"
		"ypos"	"16"
		"wide"	"160"
		"tall"  "160"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"

	}
	CHudResources
	{
		"fieldName"		"CHudResources"
		"xpos"	"100"
		"ypos"	"14"
		"wide"	"160"
		"tall"  "160"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"

	}
	CHealthBarPanel
	{
		"fieldName"		"CHealthBarPanel"
		"xpos"	"200"
		"ypos"	"14"
		"wide"	"160"
		"tall"  "160"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"

	}
	VehicleRoleHudElement
  {
		"fieldName"		"VehicleRoleHudElement"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HudTimer
	{
		"fieldName"		"HudTimer"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	TargetID
	{
		"fieldName"		"TargetID"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HudResourcesPickup
	{
		"fieldName"		"HudResourcesPickup"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HudResources
	{
		"fieldName"		"HudResources"
		"xpos"	"50"
		"ypos"	"1"
		"wide"	"102"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"
		"ResourcesLabel" 	"Resources"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HudOrderList
	{
		"fieldName"		"HudOrderList"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HudVehicleHealth
	{
		"fieldName"		"HudVehicleHealth"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	DamageIndicator
	{
		"fieldName"		"DamageIndicator"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HealthLabel
	{
		"fieldName"		"HealthLabel"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HudAmmoSecondary
	{
		"fieldName"		"HudAmmoSecondary"
		"xpos"	"16"
		"ypos"	"2000"
		"wide"	"35"
		"tall"  "36"
		"visible" "0"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}

	HudAmmoPrimaryClip
	{
		"fieldName" "HudAmmoPrimaryClip"
		"xpos"	"r120"
		"ypos"	"432"
		"wide"	"150"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"

		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "28"
		"digit_ypos" "2"
		"digit2_xpos" "78"
		"digit2_ypos" "16"
	}
	HudAmmoPrimary
	{
		"fieldName"		"HudAmmoPrimary"
		"xpos"	"1000"
		"ypos"	"425"
		"wide"	"102"
		"tall"  "20"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "2"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	HudWeaponFlashHelper
	{
		"fieldName"		"HudWeaponFlashHelper"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	
	
	HudHealth
	{
		"fieldName"		"HudHealth"
		"xpos"	"16"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}

	CHudArmor
	{
		"fieldName"		"CHudArmor"
		"xpos"	"120"
		"ypos"	"432"
		"wide"	"102"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}
	
	HudSuit
	{
		"fieldName"		"HudSuit"
		"xpos"	"140"
		"ypos"	"432"
		"wide"	"108"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"

		
		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "50"
		"digit_ypos" "2"
	}

	CHudProgressBar
	{
		"fieldName" "CHudProgressBar"
		"xpos"	"c-150"
		"ypos"	"300"
		"wide"	"300"
		"tall"  "15"
		"visible" "1"
		"enabled" "1"

		"BorderThickness" "1"

		"PaintBackgroundType"	"2"
	}

	CHudRoundTimer
	{
		"fieldName" "CHudRoundTimer"
		"xpos"	"c50"
		"ypos"	"432"
		"wide"	"170"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"

		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "44"
		"digit_ypos" "2"
		//"NumberFont" "CloseCaption_Bold"
		//"digit_xpos" "3"
		//"digit_ypos" "1"
	}

	CHudAccount
	{
		"fieldName" "CHudAccount"
		"xpos"	"r100"
		"ypos"	"390"
		"wide"	"100"
		"tall"  "36"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"

		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "24"
		"digit_ypos" "2"
		//"NumberFont" "CloseCaption_Bold"
		//"digit_xpos" "3"
		//"digit_ypos" "1"
	}

	CHudShoppingCart
	{
		"fieldName" "CHudShoppingCart"
		"xpos"	"10"
		"ypos"	"290"
		"wide"	"64"
		"tall"  "64"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
	}

	CHudC4
	{
		"fieldName" "CHudC4"
		"xpos"	"10"
		"ypos"	"226"
		"wide"	"64"
		"tall"  "64"
		"visible" "0"
		"enabled" "0"

		"PaintBackgroundType"	"2"
	}

	HudAmmo
	{
		"fieldName" "HudAmmo"
		"xpos"	"r120"
		"ypos"	"432"
		"wide"	"110"
		"tall"  "36"
		"visible" "1"
		"enabled" "1"

		"PaintBackgroundType"	"2"

		"text_xpos" "8"
		"text_ypos" "20"
		"digit_xpos" "28"
		"digit_ypos" "2"
		"digit2_xpos" "78"
		"digit2_ypos" "16"
	}

	HudSuitPower
	{
		"fieldName" "HudSuitPower"
		"visible" "1"
		"enabled" "1"
		"xpos"	"16"
		"ypos"	"396"
		"wide"	"102"
		"tall"	"30"
		
		"AuxPowerLowColor" "255 0 0 220"
		"AuxPowerHighColor" "255 220 0 220"
		"AuxPowerDisabledAlpha" "70"

		"BarInsetX" "8"
		"BarInsetY" "8"
		"BarWidth" "92"
		"BarHeight" "4"
		"BarChunkWidth" "6"
		"BarChunkGap" "3"

		"text_xpos" "8"
		"text_ypos" "15"

		"PaintBackgroundType"	"2"
	}
	
	HudFlashlight
	{
		"fieldName" "HudFlashlight"
		"visible" "0"
		"enabled" "0"
		"xpos"	"0"
		"ypos"	"0"
		"wide"	"0"
		"tall"	"0"
		
//		"text_xpos" "10"
//		"text_ypos" "5"
//		"TextColor"	"255 170 0 100"

		"PaintBackgroundType"	"2"
	}
	
	HudDamageIndicator
	{
		"fieldName" "HudDamageIndicator"
		"visible" "1"
		"enabled" "1"
		"DmgColorLeft" "255 0 0 0"
		"DmgColorRight" "255 0 0 0"
		
		"dmg_xpos" "30"
		"dmg_ypos" "100"
		"dmg_wide" "36"
		"dmg_tall1" "240"
		"dmg_tall2" "200"
	}

	HudZoom
	{
		"fieldName" "HudZoom"
		"visible" "0"
		"enabled" "0"
		"Circle1Radius" "66"
		"Circle2Radius"	"74"
		"DashGap"	"16"
		"DashHeight" "4"
		"BorderThickness" "88"
	}

	HudWeaponSelection
	{
		fieldName			HudWeaponSelection
		xpos				15
		ypos				20
		wide				700
		tall				400
		visible 			1
		enabled				1

		WeaponMenu1
		{
			wide		80
			tall		60
			xpos		-3
			ypos		10
			TextYPos	54
			enabled	1
			visible	1
		}

		WeaponMenu2
		{
			wide		80
			tall		60
			xpos		-3
			ypos		74
			enabled	1
			visible	1
		}

		WeaponMenu3
		{
			wide		80
			tall		60
			xpos		-3
			ypos		138
			enabled	1
			visible	1
		}

		WeaponMenu4
		{
			wide		80
			tall		60
			xpos		-3
			ypos		202
			enabled	1
			visible	1
		}

		WeaponMenu5
		{
			wide		80
			tall		60
			xpos		-3
			ypos		266
			enabled	1
			visible	1
		}

		WeaponMenu6
		{
			wide		80
			tall		60
			xpos		-3
			ypos		330
			enabled	1
			visible	1
		}

		BuildMenu1
		{
			wide		80
			tall		60
			xpos		100
			visible	1
		}

		BuildMenu2
		{
			wide		80
			tall		60
			xpos		200
			visible	1
		}

		BuildMenu3
		{
			wide		80
			tall		60
			xpos		300
			visible	1
		}

		BuildMenu4
		{
			wide		80
			tall		60
			xpos		400
			visible	1
		}

		BuildMenu5
		{
			wide		80
			tall		60
			xpos		500
			visible	1
		}

		BuildMenu6
		{
			wide		80
			tall		60
			xpos		600
			visible	1
		}
	}

	HudCrosshair
	{
		"fieldName" "HudCrosshair"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudDeathNotice
	{
		"fieldName" "HudDeathNotice"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudVehicle
	{
		"fieldName" "HudVehicle"
		"visible" "0"
		"enabled" "0"
		"wide"	 "640"
		"tall"	 "480"
	}

	CVProfPanel
	{
		"fieldName" "CVProfPanel"
		"visible" "0"
		"enabled" "0"
		"wide"	 "640"
		"tall"	 "480"
	}

	ScorePanel
	{
		"fieldName" "ScorePanel"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudTrain
	{
		"fieldName" "HudTrain"
		"visible" "0"
		"enabled" "0"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudMOTD
	{
		"fieldName" "HudMOTD"
		"visible" "0"
		"enabled" "0"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudMessage
	{
		"fieldName" "HudMessage"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudMenu
	{
		"fieldName" "HudMenu"
		"visible" "0"
		"enabled" "0"
		"wide"	 "640"
		"tall"	 "480"
	}
	
	HudVoiceSelfStatus
	{
		"fieldName" "HudVoiceSelfStatus"
		"visible" "1"
		"enabled" "1"
		"xpos" "r34"
		"ypos" "355"
		"wide" "24"
		"tall" "24"
	}

	HudVoiceStatus
	{
		"fieldName" "HudVoiceStatus"
		"visible" "1"
		"enabled" "1"
		"xpos" "r200"
		"ypos" "0"
		"wide" "200"
		"tall" "400"

		"item_tall"	"16"
		"item_wide"	"195"
		"item_spacing" "2"
		
		"show_avatar"		"1"
		"show_friend"		"1"
		"show_voice_icon"	"0"
		"show_dead_icon"	"1"

		"dead_xpos"	"0"
		"dead_ypos"	"0"
		"dead_wide"	"16"
		"dead_tall"	"16"

		"avatar_xpos"	"14"
		"avatar_ypos"	"0"
		"avatar_wide"	"16"
		"avatar_tall"	"16"
		
		"text_xpos"	"42"
		
		"fade_in_time" "0.07"
		"fade_out_time" "1.0"
	}

	HudCloseCaption
	{
		"fieldName" "HudCloseCaption"
		"visible" "0"
		"enabled" "0"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudChat
	{
		"fieldName" "HudChat"
		"visible" "1"
		"enabled" "1"
		"xpos"	"c-200"
		"ypos"	"300"
		"wide"	 "400"
		"tall"	 "100"
	}
	
	HudRadar
	{
		"fieldName" "HudRadar"
		"visible"	"1"
		"enabled"	"1"
		"xpos"		"16"
		"ypos"		"16"
		"wide"		"96"
		"tall"		"96"
	}

	HudHistoryResource
	{
		"fieldName" "HudHistoryResource"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudGeiger
	{
		"fieldName" "HudGeiger"
		"visible" "0"
		"enabled" "0"
		"wide"	 "640"
		"tall"	 "480"
	}

	HUDQuickInfo
	{
		"fieldName" "HUDQuickInfo"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}

	HudWeapon
	{
		"fieldName" "HudWeapon"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}
	HudAnimationInfo
	{
		"fieldName" "HudAnimationInfo"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}
	CBudgetPanel
	{
		"fieldName" "CBudgetPanel"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}
	CTextureBudgetPanel
	{
		"fieldName" "CTextureBudgetPanel"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}
	
	HudPredictionDump
	{
		"fieldName" "HudPredictionDump"
		"visible" "1"
		"enabled" "1"
		"wide"	 "640"
		"tall"	 "480"
	}
}