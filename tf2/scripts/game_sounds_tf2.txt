// OMG TF2
// Andy says:	Footstep sounds are better off in a separate file ( game_sounds_tf2_footsteps.txt )
//		so they don't take up half the space in here.

"ResourceChunk.Pickup"
{
	"channel" 	"CHAN_ITEM"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave" 		"tf2/resourcepickup.wav"
}
"Humans.Death"
{
	"channel" 	"CHAN_VOICE"
	"volume" 		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave"		"tf2/humans/pain01.wav"
	"wave"		"tf2/humans/pain02.wav"
	"wave"		"tf2/humans/pain03.wav"
	"wave"		"tf2/humans/pain04.wav"
	"wave"		"tf2/humans/pain05.wav"
	"wave"		"tf2/humans/pain06.wav"
	"wave"		"tf2/humans/pain07.wav"
	"wave"		"tf2/humans/pain08.wav"
	"wave"		"tf2/humans/pain09.wav"
}
"AlienCommando.Death"
{
	"channel" 	"CHAN_ITEM"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave" 		"tf2/aliens/die1.mp3"
	"wave" 		"tf2/aliens/die2.mp3"
}
"AlienMedic.Death"
{
	"channel" 	"CHAN_ITEM"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave" 		"tf2/aliens/die1.mp3"
	"wave" 		"tf2/aliens/die2.mp3"
}
"AlienDefender.Death"
{
	"channel" 	"CHAN_ITEM"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave" 		"tf2/aliens/die1.mp3"
	"wave" 		"tf2/aliens/die2.mp3"
}
"AlienEscort.Death"
{
	"channel" 	"CHAN_ITEM"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave" 		"tf2/aliens/die1.mp3"
	"wave" 		"tf2/aliens/die2.mp3"
}
"Laserrifle.Fire"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/laserfire.wav"
}
"NPC_Bug_Warrior.Idle"
{
	"channel" 	"CHAN_VOICE"
	"volume" 		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave"		"tf2/warrior/idle.wav"
}
"NPC_Bug_Warrior.Pain"
{
	"channel" 	"CHAN_VOICE"
	"volume" 		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave"		"tf2/warrior/pain1.wav"
	"wave"		"tf2/warrior/pain2.wav"
	"wave"		"tf2/warrior/pain3.wav"
}
"BaseTFPlayer.BloodSportKiller"
{
	"channel" 	"CHAN_VOICE"
	"volume" 		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave"		"tf2/RampageKill.wav"
}
"WeaponObjectSapper.Attach"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/SapperAttach.wav"
}
"Shotgun.Fire"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/shotgunfire.wav"
}
"TF2.Reload"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"wave" 		"tf2/Reload.wav"
}
"Burstrifle.Fire"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/burstriflefire.wav"
}
"Plasmarifle.Fire"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/plasmariflefire.wav"
}
"Minigun.Fire"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"

	"rndwave"
{
	"wave" 		"tf2/hmg1_1.wav"
	"wave" 		"tf2/hmg1_2.wav"
	"wave" 		"tf2/hmg1_3.wav"
	"wave" 		"tf2/hmg1_7.wav"
	"wave" 		"tf2/hmg1_8.wav"
	"wave" 		"tf2/hmg1_9.wav"
}
}
"Rocketlauncher.Fire"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/rocketfire1.wav"
}
"Grenadelauncher.Fire"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/Grenadelauncherfire.wav"
}
"Commando.BootHit"
{
	"channel" 	"CHAN_VOICE"
	"volume" 		"0.90"
	"soundlevel" 	"SNDLVL_75dB"

	"rndwave"
	{
	"wave"		"tf2/BootKick.wav"
	"wave"		"tf2/BootKick2.wav"
	"wave"		"tf2/BootKick3.wav"
	}
}
"WeaponRepairGun.NoTarget"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/RepairgunNotarget.wav"
}
"WeaponRepairGun.Healing"
{
	"channel" 	"CHAN_WEAPON"
	"volume"		"0.90"
	"soundlevel" 	"SNDLVL_GUNFIRE"

	"wave" 		"tf2/RepairgunHealing.wav"
}