// aVe teh_lolweap

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"        "ArcWelder"
	"viewmodel"        "models/weapons/v_tf2_shield.mdl"
	"playermodel"      "models/weapons/w_human_shield.mdl"	//FIXME: 
	"anim_prefix"      "avegun"
	"bucket"           "2"
	"bucket_position"  "0"

	"clip_size"        "30"
	"clip2_size"       "-1"

	"default_clip"     "30"
	"default_clip2"    "-1"

	"primary_ammo"     "Rockets"

	"weight"           "2"
	"item_flags"       "0"

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"hud/menu/human_shield"
				"x"		"0"
				"y"		"0"
				"width"		"128"
				"height"	"64"
		}
		"weapon_s"
		{
				"file"		"hud/menu/human_shield"
				"x"		"0"
				"y"		"0"
				"width"		"128"
				"height"	"64"
		}
		"ammo"
		{
				"file"    "sprites/a_icons1"
				"x"       "55"
				"y"       "60"
				"width"   "73"
				"height"  "15"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			  "0"
				"y"       "48"
				"width"   "24"
				"height"  "24"
		}
		"autoaim"
		{
				"file"    "sprites/crosshairs"
				"x"       "0"
				"y"       "48"
				"width"   "24"
				"height"  "24"
		}
	}
}
