"RespawnWave.res"
{
    "Background"
    {
        "ControlName"   "MaterialImage"
        "fieldName"     "Background"
        "xpos"          "0"
        "ypos"          "0"
        "zpos"          "-2"
        "wide"          "480"
        "tall"          "240"

        "material"      "vgui/TF2Selections/vgui_bg"
    }

    "lblTime"
    {
        "ControlName"   "Label"
        "fieldName"     "lblTime"
        "xpos"          "10"
        "ypos"          "20"
        "wide"          "240"
        "tall"          "34"
        "autoResize"    "0"
        "pinCorner"     "0"
        "visible"       "1"
        "enabled"       "1"
        "tabPosition"   "0"
        "labelText"     "Time remaining:"
        "textAlignment" "center"
        "dulltext"      "0"
        "paintBackground" "0"
    }

    "RespawnTime1Remaining"
    {
        "ControlName"   "Label"
        "fieldName"     "RespawnTime1Remaining"
        "xpos"          "240"
        "ypos"          "20"
        "wide"          "240"
        "tall"          "34"
        "autoResize"    "0"
        "pinCorner"     "0"
        "visible"       "1"
        "enabled"       "1"
        "tabPosition"   "0"
        "labelText"     "0"
        "textAlignment" "center"
        "dulltext"      "0"
        "paintBackground" "0"
    }
}